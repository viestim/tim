import angular from "angular";

export const jsrunnerApp = angular.module("jsrunnerApp", ["ngSanitize"]);
