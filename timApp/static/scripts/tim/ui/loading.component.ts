import {Component} from "@angular/core";

@Component({
    selector: "tim-loading",
    template: `
<i class="glyphicon glyphicon-refresh"></i>
    `,
    styleUrls: ["./loading.component.scss"],
})
export class LoadingComponent {}
