timApp.bookmark package
=======================

Submodules
----------

timApp.bookmark.bookmarks module
--------------------------------

.. automodule:: timApp.bookmark.bookmarks
    :members:
    :undoc-members:
    :show-inheritance:

timApp.bookmark.routes module
-----------------------------

.. automodule:: timApp.bookmark.routes
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: timApp.bookmark
    :members:
    :undoc-members:
    :show-inheritance:
