timApp.folder package
=====================

Submodules
----------

timApp.folder.folder module
---------------------------

.. automodule:: timApp.folder.folder
    :members:
    :undoc-members:
    :show-inheritance:

timApp.folder.folder\_view module
---------------------------------

.. automodule:: timApp.folder.folder_view
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: timApp.folder
    :members:
    :undoc-members:
    :show-inheritance:
