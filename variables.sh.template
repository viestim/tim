#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo variables.sh: Fill in appropriate variable values and comment this line. ; exit 1

# Hostname for the TIM instance.
export TIM_HOST=http://localhost

# Domain(s) for Caddy server (separate with comma and space).
# By default, Caddy domain has no host so that IPs work too (in addition to localhost).
# This is important when connecting to local TIM with other devices such as a mobile phone.
# For production, replace with your server domain:
# export CADDY_DOMAINS='domain.com'
export CADDY_DOMAINS='http://'

# If COMPOSE_PROFILES=prod_multi, specifies the host port for Caddy. Otherwise unused.
export CADDY_MULTI_PORT=50000

# Extra configuration to add to Caddyfile.
export CADDY_EXTRA_CONFIG='

'

# TIM image tag to use. Do not modify; it is computed automatically.
export TIM_IMAGE_TAG=$(${DIR}/get_latest_date.sh)

# Maximum number of allowed connections to PostgreSQL.
export PG_MAX_CONNECTIONS=200

# Size for /dev/shm for PostgreSQL container.
export PG_SHM_SIZE=64mb

# Name of the Docker Compose project. Modify only if there are multiple TIM instances in the same host.
export COMPOSE_PROJECT_NAME=tim

# TIM root directory; do not modify.
export TIM_ROOT=${DIR}

# Root directory for non-db storage (document content, uploaded files, etc.).
export FILES_ROOT=${DIR}/timApp/tim_files

# Possible values: prod, prod_multi, dev, test
# Explanations:
# * prod: This is a single TIM instance running on a remote machine.
# * prod_multi: This is one of multiple TIM instances running on the same remote machine.
# * dev: This is a local development TIM instance.
# * test: This instance is used only for running tests. Used mostly in GitLab CI.
export COMPOSE_PROFILES=prod

# Command to start csplugin when COMPOSE_PROFILES=dev; use empty command if you want to debug it with SSH.
export CSPLUGIN_DEV_COMMAND=./startPlugins.sh

# Same as above but for textfield plugin.
export TEXTFIELD_DEV_COMMAND=./startAll.sh

# Same as above but for jsrunner plugin.
export JSRUNNER_DEV_COMMAND=./startAll.sh

# Same as above but for svn plugin.
export SVNPLUGIN_DEV_COMMAND=./startAll.sh

# Same as above but for pali plugin.
export PALI_DEV_COMMAND=./startAll.sh

# Same as above but for feedback plugin.
export FEEDBACK_DEV_COMMAND=./startAll.sh

# Same as above but for drag plugin.
export DRAG_DEV_COMMAND=./startAll.sh

# Same as above but for imagex plugin.
export IMAGEX_DEV_COMMAND=./startAll.sh

# Location of config file to use in addition to defaultconfig (relative to timApp directory).
# In production, you need to create the prodconfig.py file.
export CONFIG_FILE='prodconfig.py'

# Location of log directory; will be mapped for TIM container at /service/tim_logs.
export LOG_DIR=${DIR}/tim_logs

# To print the concrete values of these variables, you can use:
# env - bash -c ". ./variables.sh && env | grep -Ev '^(_|PWD|SHLVL)='"

# The COMPOSE_PARALLEL_LIMIT is probably not required to be adjusted, but it's here just for reference.
# export COMPOSE_PARALLEL_LIMIT=10
